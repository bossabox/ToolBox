# ToolBox

Lista e breve explicação das ferramentas utilizadas na metodologia desenvolvida para os produtos da BossaBox.


ToolBBox

Para os Squads

**1.	Compartilhamento Arquivos: Google Drive**   
	Todo e qualquer arquivo que possa ser relevante para o projeto deve ser salvo no Google Drive. 
	“BossaBox” --> “Clientes” --> “(Nome_Cliente)” *nessa pasta, deixar assuntos relacionados à conta do cliente* --> “Produto”*convidar todos os players --> “Design”, “Desenvolvimento”.
	Todos devem fazer muito uso desse recurso.

**2.	Chat: Slack**   
    Todos os produtos a serem construídos devem ter um Workspace correspondente – mesmo que um cliente tenha mais de um produto, deve-se criar um Workspace para cada produto.
    Deve constar os seguintes channels:
    
    #general --> Notificações do Slack (Apps e Slackbot) ocorrem aqui e é aonde devemos realizar toda comunicação entre Cliente e algum membro externo ao Núcleo de Produto (Designers e Devs não-líderes) – pode ser dita alguma coisa que importa para outro Prolancer.
    #prolancers --> Todos, menos o Cliente. É nesse Canal que os núcleos dialogam entre si. Tudo que for requisição de um núcleo para o outro, de acordo com o SLA existente entre eles, deve ser solucionado aqui.
    #product --> Cliente, Coordenador e Líderes. Canal para decisões estratégicas sobre o produto e refinamento do Backlog. É o principal canal que Cliente e Coordenador utilizarão ao longo do release.
    #design --> Líder UX e Designers UI. Canal para decisões de tudo a que se refere a produção de Mockups e Protótipos.
    #development --> Líder Dev e Devs FS. Canal para decisões a tudo que se refere a Código, Arquitetura do Software e DevOps.
    
    Todos devem fazer muito uso desse recurso.
    
    
**3.	Ligações: Appear.in**/Zoom  
	Plataforma para Video-Conferência online (100% Web, Até 4 pessoas de graça, Screensharing, app iOS, Salas Privadas).
	Deve haver um Link com o seguinte nome, para cada Channel:
	
	#general --> Appear.in/bossabox-(Nome_Projeto)-general
	#prolancers --> Appear.in/bossabox-(Nome_Projeto)-prolancers
	#product --> Appear.in/bossabox-(Nome_Projeto)-product
	#design --> Appear.in/bossabox-(Nome_Projeto)-design
	#development --> Appear.in/bossabox-(Nome_Projeto)-development
	
	*Zoom está sendo testado pela equipe de vendas. Dependendo do resultado, será 	implementado.
	
    Todos devem fazer muito uso desse recurso.

**4.	Coleta de Dados: Typeform**
	Envio dos Typeforms com a URL correspondente. 
	Transcrição das Respostas para os Artefatos correspondentes, com upload posterior no Google Drive.
	Todos os Typeforms serão salvos na conta da Bossa e estarão disponíveis para acesso pelo time. 
	O Coordenador e o Cliente devem fazer muito uso desse recurso.

**5.	Compartilhamento de senhas:** Okta/Dashlane*    
*Estudar

**6.	Fluxogramas: LucidChart**   
    Documentação dos Processos da Plataforma. 
    •	Um PDF resultante da versão mais atual do processo deve ser enviado no processo de Onboarding de cada um dos players envolvidos.
    Documentação dos Processos do Modelo de Negócios do Cliente, que não compele ao Produto. Ótimo para contextualizar o Produto na grande estratégia do Cliente, propiciando uma análise de como o Produto irá criar valor para o cliente.
    O Coordenador irá fazer muito uso desse recurso.

**7.	Story Mapping: Stories on Board**   
    Ferramenta para visualização da estratégia do Produto. Apesar de compartilhada e utilizada com o time todo, trata-se da principal ferramenta do Squad de Produto. É aqui que as decisões mais estratégicas são tomadas e as grandes funcionalidades devem ser esclarecidas. 
    Organizado Verticalmente em:
    1.	Usuário
    2.	Objetivo (Steps)
    3.	Epics (Steps)
    4.	USs (Steps)
    5.	Releases --> Como consequência da Priorização
    
    Organizado Horizontalmente de acordo com o sentido lógico para o usuário/para o software.
    O Coordenador e o Cliente devem fazer muito uso desse recurso.

**8.	Gestão de Tarefas: Trello**     
    Ferramenta visual para Gestão de Equipes Remotas.
    Colunas: READ.ME, “Blocked”/“Product Backlog”, “In Refinement”, “Ready to Dev”, “To Do/Sprint Backlog”, Doing, Waiting Review, Done. 
    Uma vez que são listadas as USs no Stories on Board, o Backlog do Release é adicionado ao Quadro Trello.
    Dashboard de Tarefas.
    O Time deve utilizar muito esse recurso.


**9.	Information Architecture (User Flows e SiteMaps): FlowMapp**    
    Ferramenta visual para a criação de User Flows e Sitemap. User Flows criam um contexto e uma conexão entre as User Stories, além de ajudar a (1) compreender melhor o software e (2) checar se não há nenhuma brecha na lógica idealizada.
    Sitemaps, por outro lado, são a conexão entre a lógica e a interface. 
    Por meio do FlowMapp, podemos facilmente começar a visualizar como os objetivos e passos que usuário irá dar ocorrerão por meio do software.
    O Líder de Design e seu Squad – posteriormente - devem utilizar muito esse recurso.

**10.	Information Architecture - Wireframing: Balsamiq**  
	Excelente ferramenta para rápida construção de Wireframes.
	Squad de Produto e de Design – posteriormente – devem utilizar muito esse recurso.

**11.	Mockups: Illustrator/Photoshop, Sketch, Adobe XD**  
    Ferramentas para produção de interfaces. Em termos de entregáveis, é pouco significativo em qual software a interface foi construída, mas é importante sempre alinhar que o time utilize as mesmas ferramentas ou, caso usem diferentes, adequem os formatos dos arquivos para aquele que os outros preferem trabalhar.
    Squad de Design deve utilizar muito esses recursos.

**12.	Prototipação: InVision, *UXPin, Adobe XD**  
    Web Apps para prototipação e colaboração. Por ora, InVision é a ferramenta preferível para o trabalho.
    Adobe XD é muito aceita no setor, então também pode ser utilizada.
    UXPin requer alguns estudos.
    Squad de Produto (Validações) e de Design (Entregas) vão utilizar esses recursos.

**13.	User Research: Lookback**   
    Excelente ferramenta para as validações, basta cadastrar um e-mail para convidar ou copiar o link do experimento.
    Utilizaremos a feature “Self-Test” do Lookback. Será utilizada para validações dos Protótipos construídos com 5 usuários que cliente irá ceder para o experimento.
    Squad de Design e Cliente vão utilizar esses recursos.

**14.	Compartilhamento MockUps/Assets/SG: *Zeplin*       
    Plataforma de compartilhamento de arquivos de Design para colaboração com Desenvolvedores.
    Uma vez aprovado o Design, deve ser realizado o upload no Zeplin para que time Dev tenha acesso.
    *Está sendo validado se InVision não tem essa funcionalidade e não irá substituir o Zeplin.
    Squad Design e Desenvolvimento vão utilizar esses recursos.	

**15.	Report Issues: Usersnap**   
	Ferramenta para a identificação, mapeamento e colaboração em Bugs/Ajustes 	necessários quando realizado o Deploy para “Staging” – Líder – e idem para quando 	estiver no “Master” – P.O./Cliente.
	Tem uma forte integração com o GitLab – envia os comentários como “Issues” 	especificados para o repositório do produto.
	Também será importante para que, ao término do Release, o Cliente possa documentar 	feedbacks do usuário sobre o Software e já deixá-los documentados no repositório.
	Muito utilizada pelo Squad de Produto.

**16.	Git Control: *GitPrime**    
    BI Software para Devs Líderes terem maior controle sobre o que está acontecendo no código. 
    *Muito cara a ferramenta e ainda precisa ser melhor estudada.
    Muito utilizada pelo Líder de Tecnologia.

**17.	DevOps/Cloud Computing: GitLab, Google Cloud Platform (Google App Engine)** 
    Processo de CI-CD do BossaBox passa muito pelas duas ferramentas.
    Haverá uma configuração inicial das ferramentas, mas o processo passa a ser extremamente automatizado uma vez passada essa etapa.
    Serão profundamente utilizadas pelo Líder de Tecnologia.

**18.	Repositório: GitLab** & *Bit    
    GitLab é o Git oficial da BossaBox. Ferramenta fantástica, em plena ascensão e com a maior parte das funcionalidades grátis
    Trabalharemos, sempre, com 2 repositórios por projeto: Front-End e Back-End.
    Deve ser utilizado como Repositório, para DevOps e correções de Issues.
    Será utilizado por todo o time de tecnologia.
    *Bit é um repositório voltado para componentes (elementos React, Vue.JS, Angular, etc). Está sendo estudado e deve ser implementado em breve, conforme a BossaBox adquire maior know-how nessa parte de componentização. 

**19.	Documentação: API Blueprint**   
	É uma linguagem de design de APIs da web. Com o API Blueprint, você pode criar 	rapidamente protótipos e modelar APIs a serem criadas ou descrever APIs já
	implementadas.
	Será muito utilizada pelo Squad de Desenvolvimento.

**20.	Teste APIs: Dredd** 
	Dredd é uma ferramenta para validar o documento de descrição da API (feito no API 	Blueprint) contra a implementação de back-end da API. O Dredd lê a descrição da sua 	API e, passo a passo, valida se a implementação da sua API responde às respostas da 	forma descrita na documentação.
	Será muito utilizada pelo Líder de Tecnologia e O Squad de desenvolvimento

*InterSquads        

**21.	Slack Geral**: Workspace de Prolancers Alocados – 1 #channel para cada stack    

**22.	Stack Overflow for Teams** – 1 para cada Stack, com Prolancers alocados 
